package morsecode

    fun decodeMorse(code: String): String {
        var result =""
        
        return code.trim()
        .split("   ")
        .joinToString(separator = " ") { word ->
            word.split(" ")
                    .map { letter -> 
                        MorseCode.getOrDefault(letter.toString(), "")  }
                    .joinToString(separator = "")
        }
        
        code.forEach { itCode ->
            if (itCode.toString() == "  ") {
                result += itCode
            }
            else {             
                result += MorseCode.getOrDefault(itCode.toString(), "").toString() 
            }
        }
        
        return result 
        }
    