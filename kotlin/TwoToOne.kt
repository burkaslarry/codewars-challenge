package twotoone

fun longest(s1:String, s2:String):String {
    // your code
    val concatString = listOf(s1, s2)
    .filterNot { it.isNullOrBlank() }
    .joinToString(separator = "")
    
    var result = "" 
    var charArray = mutableListOf<Char>()
    concatString.forEach { char ->        
        charArray.add(char)
    }    
    
    charArray.sort()
    result = charArray.distinct().joinToString(separator = "") 
    print("result : $result")
    
    return result 
}