package morsecode

    fun decodeMorse(code: String): String {
     
        
        return code.trim()
        .split("   ")
        .joinToString(separator = " ") { word ->
            word.split(" ")
                    .map { letter -> 
                        MorseCode.getOrDefault(letter.toString(), "")  }
                    .joinToString(separator = "")
        }
       
        }
    